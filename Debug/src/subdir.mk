################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bldc.c \
../src/stm32f1xx_it.c \
../src/syscalls.c \
../src/system_stm32f1xx.c 

CPP_SRCS += \
../src/main.cpp \
../src/setup.cpp 

OBJS += \
./src/bldc.o \
./src/main.o \
./src/setup.o \
./src/stm32f1xx_it.o \
./src/syscalls.o \
./src/system_stm32f1xx.o 

C_DEPS += \
./src/bldc.d \
./src/stm32f1xx_it.d \
./src/syscalls.d \
./src/system_stm32f1xx.d 

CPP_DEPS += \
./src/main.d \
./src/setup.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103RCTx -DDEBUG -DSTM32F103xE -DUSE_HAL_DRIVER -I"/home/ivliev/workspace/hower/HAL_Driver/Inc/Legacy" -I"/home/ivliev/workspace/hower/inc" -I"/home/ivliev/workspace/hower/CMSIS/device" -I"/home/ivliev/workspace/hower/CMSIS/core" -I"/home/ivliev/workspace/hower/HAL_Driver/Inc" -I/home/ivliev/workspace/hower/ros_lib -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU G++ Compiler'
	@echo $(PWD)
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103RCTx -DDEBUG -DSTM32F103xE -DUSE_HAL_DRIVER -I"/home/ivliev/workspace/hower/HAL_Driver/Inc/Legacy" -I"/home/ivliev/workspace/hower/inc" -I"/home/ivliev/workspace/hower/CMSIS/device" -I"/home/ivliev/workspace/hower/CMSIS/core" -I"/home/ivliev/workspace/hower/HAL_Driver/Inc" -I/home/ivliev/workspace/hower/ros_lib -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fno-exceptions -fno-rtti -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


