################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../startup/startup_stm32.s 

OBJS += \
./startup/startup_stm32.o 


# Each subdirectory must supply rules for building sources it contributes
startup/%.o: ../startup/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Assembler'
	@echo $(PWD)
	arm-none-eabi-as -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -I"/home/ivliev/workspace/hower/HAL_Driver/Inc/Legacy" -I"/home/ivliev/workspace/hower/inc" -I"/home/ivliev/workspace/hower/CMSIS/device" -I"/home/ivliev/workspace/hower/CMSIS/core" -I"/home/ivliev/workspace/hower/HAL_Driver/Inc" -I/home/ivliev/workspace/hower/ros_lib -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


